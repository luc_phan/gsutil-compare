gsutil-compare
==============

Description
-----------

`gsutil-compare` is a tool to compare 2 buckets of **Google Storage**.

Usage
-----

```
$ gsutil-compare project-1 gs://path-1 project-2 gs://path-2
...
Missing in project-1:
Missing in project-2:
- Directory1/
- Directory2/
Mismatching files:
```

Installation
------------

```
$ pip install gsutil-compare-0.1.0.tar.gz
```

Development
-----------

### Test

```
$ python setup.py develop
$ gsutil-compare
```

### Argument parsing

- ~~argparse~~
- ~~docopt~~
- click

### TODO

- ~~Refactor: Use recursive structure to prevent displaying too much differences~~
- ~~Refactor: Recursive algorithm too slow~~
- ~~Doc~~
