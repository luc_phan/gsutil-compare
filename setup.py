from setuptools import setup

setup(
    name='gsutil-compare',
    version='0.1.0',
    packages=['gsutilcompare'],
    url='',
    license='',
    author='Luc PHAN',
    author_email='',
    description='',
    entry_points={'console_scripts': ['gsutil-compare = gsutilcompare.command:main']},
    install_requires=['click', 'pyyaml']
)
