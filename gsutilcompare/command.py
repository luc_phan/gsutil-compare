import click
import subprocess
import re
import yaml


@click.command()
@click.argument('project1')
@click.argument('path1')
@click.argument('project2')
@click.argument('path2')
def main(project1, path1, project2, path2):
    command = Command()
    command.compare(project1, path1, project2, path2)


class Command:

    def __init__(self):
        self._path_re = re.compile(r'^(\S.*):$')
        self._content_length_re = re.compile(r'Content-Length:\s*(\d+)')
        self._crc32c_re = re.compile(r'Hash \(crc32c\):\s*(\S+)')
        self._md5_re = re.compile(r'Hash \(md5\):\s*(\S+)')
        self._etag_re = re.compile(r'ETag:\s*(\S+)')

    def compare(self, project1, path1, project2, path2):
        self._set_project(project1)
        tree1 = self._extract_tree3(path1)
        print(yaml.dump(tree1))

        self._set_project(project2)
        tree2 = self._extract_tree3(path2)
        print(yaml.dump(tree2))

        diff = self._diff(project1, tree1, project2, tree2)
        print(yaml.dump(diff))

        self._display_diff(diff)

    def _set_project(self, project):
        self._run(['gcloud', 'config', 'set', 'project', project])

    def _extract_tree3(self, path):
        if not self._exists(path):
            raise LookupError("Path doesn't seem to exist: " + path)

        cleaned_path = self._clean_path(path)
        normalized_path = self._normalize_path(path)
        tree = dict(type='directory', content={})
        node = None

        returncode, stdout, stderr = self._run(['gsutil', 'ls', '-RL', cleaned_path])
        for line in stdout.splitlines():

            m = self._path_re.match(line)
            if m:
                subpath = m.group(1)
                relpath = self._get_relative_path(subpath, normalized_path)
                node = self._store_path(relpath, subpath, tree)

            m = self._content_length_re.search(line)
            if m:
                content_length = m.group(1)
                node['content_length'] = int(content_length)

            m = self._crc32c_re.search(line)
            if m:
                crc32c = m.group(1)
                node['crc32c'] = crc32c

            m = self._md5_re.search(line)
            if m:
                md5 = m.group(1)
                node['md5'] = md5

            m = self._etag_re.search(line)
            if m:
                etag = m.group(1)
                node['etag'] = etag

        return tree

    def _clean_path(self, path):
        if path.endswith('/'):
            return path[:-1]
        return path

    def _store_path(self, relpath, path, tree):
        destination = tree
        components = self._split_path(relpath)
        for component in components[:-1]:
            destination = destination['content'][component]

        last_component = components[-1]
        if path.endswith('/'):
            node = dict(
                type='directory',
                relpath=relpath,
                path=path,
                content=dict()
            )
        else:
            node = dict(
                type='file',
                relpath=relpath,
                path=path
            )

        destination['content'][last_component] = node
        return node

    def _split_path(self, path):
        if path.endswith('/'):
            path = path[:-1]
        return path.split('/')

    def _normalize_path(self, path):
        if self._is_directory(path) and path[-1] != '/':
            return path + '/'
        return path

    def _exists(self, path):
        returncode, stdout, stderr = self._run_process(['gsutil', 'ls', '-d', path])
        return returncode == 0

    def _is_file(self, path):
        returncode, stdout, stderr = self._run(['gsutil', 'ls', '-d', path])
        for line in stdout.splitlines():
            if line == path and not line.endswith('/'):
                return True
        return False

    def _is_directory(self, path):
        returncode, stdout, stderr = self._run(['gsutil', 'ls', '-d', path])
        for line in stdout.splitlines():
            if self._line_match_path(line, path) and line.endswith('/'):
                return True
        return False

    def _get_relative_path(self, subpath, path):
        if not subpath.startswith(path):
            raise ValueError("Sub-path mismatch: {} ({})".format(subpath, path))
        return subpath[len(path):]

    def _line_match_path(self, line, path):
        return line[:len(path)] == path and (line[len(path):] == '/' or line[len(path):] == '')

    def _run(self, command):
        returncode, stdout, stderr = self._run_process(command)
        if returncode:
            error_message = "Failed to execute: " + ' '.join(command)
            print('***', error_message)
            print("- Return code:", returncode)
            print("- STDOUT:", stdout)
            print("- STDERR:", stderr)
            raise RuntimeError(error_message)
        return returncode, stdout, stderr

    def _run_process(self, command):
        print("Executing:", command)
        proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate()
        return proc.returncode, stdout.decode('utf-8'), stderr.decode('utf-8')

    def _diff(self, project1, tree1, project2, tree2):
        missing_nodes1 = self._get_missing_nodes(project1, tree1, project2, tree2)
        missing_nodes2 = self._get_missing_nodes(project2, tree2, project1, tree1)
        mismatch_files = self._get_mismatch_files(project1, tree1, project2, tree2)

        return dict(
            missing={project2: missing_nodes1, project1: missing_nodes2},
            mismatch=mismatch_files
        )

    def _get_missing_nodes(self, project1, tree1, project2, tree2):
        missing_nodes = list()

        for name, node in tree1['content'].items():
            if name not in tree2['content']:
                print("*** Missing:", name)
                missing_nodes.append(node)
                continue

            if node['type'] == 'directory':
                missing_subnodes = self._get_missing_nodes(
                    project1, tree1['content'][name], project2, tree2['content'][name]
                )
                missing_nodes += missing_subnodes

        return missing_nodes

    def _display_diff(self, diff):
        for project, nodes in diff['missing'].items():
            print("Missing in {}:".format(project))
            for node in nodes:
                print('-', node['relpath'])

        print("Mismatching files:")
        for node in diff['mismatch']:
            print('-', node['relpath'])

    def _get_mismatch_files(self, project1, tree1, project2, tree2):
        mismatch_files = list()

        for name, node1 in tree1['content'].items():
            if name not in tree2['content']:
                continue

            if node1['type'] == 'file':
                node2 = tree2['content'][name]
                if self._mismatch(node1, node2):
                    mismatch_files.append(node1)
            elif node1['type'] == 'directory':
                mismatch_subfiles = self._get_mismatch_files(
                    project1, tree1['content'][name], project2, tree2['content'][name]
                )
                mismatch_files += mismatch_subfiles
            else:
                raise ValueError("Unknown node type: " + node1['type'])

        return mismatch_files

    def _mismatch(self, node1, node2):
        for key in ('content_length', 'crc32c', 'md5'):
            if node1[key] != node2[key]:
                print(
                    "*** Value mismatch: {} {} {} != {}".format(
                        node1['relpath'],
                        key,
                        node1[key],
                        node2[key]
                    )
                )
                return True
        return False
